#pragma once
#include "alias.hpp"
#include "token.hpp"
namespace COM1::SIN{
		enum class action {
			error,
			accept,//return 0
			shift, //go to other state
			reduce, // print production
			none
		};

		opt<size_t> transitionNumber(token &tk) {
			if (tk.getName() == "keyword") {
				if (tk.getLexema() == "inicio") return 0;
				if (tk.getLexema() == "varinicio") return 1;
				if (tk.getLexema() == "varfim") return 2;
				if (tk.getLexema() == "leia") return 8;
				if (tk.getLexema() == "escreva") return 9;
				if (tk.getLexema() == "se") return 14;
				if (tk.getLexema() == "entao") return 17;
				if (tk.getLexema() == "fimse") return 19;
				if (tk.getLexema() == "fim") return 20;
				if (tk.getLexema() == "inteiro") return 5;
				if (tk.getLexema() == "literal") return 7;
				if (tk.getLexema() == "real") return 6;
			}
			if (tk.getName() == "Numero") return 11;
			if (tk.getName() == "Literal") return 10; //atenção, existem 2 "literal" um é keyword, outro token
			if (tk.getName() == "FC_P") return 16;
			if (tk.getName() == "ID") return 4;
			if (tk.getName() == "PT_V") return 3;
			if (tk.getName() == "EOF") return 21;
			if (tk.getName() == "OPR") return 18;
			if (tk.getName() == "RCB") return 12;
			if (tk.getName() == "OPM") return 13;
			if (tk.getName() == "AB_P") return 15;
			return std::nullopt;
		}


		class sintaticError{
			constexpr static std::pair<size_t, const char*> _error[] {
					{0, "'inicio' eperado, recebido "},
					{1, "'EOF' esperado, recebido "},
					{2,"'varinicio' esperado, recebido "},
					{3,"'varfim' ou 'id' esperado, recebido "},
					{4,"'fim', 'leia', 'escreva', 'id' ou 'se' esperado, recebido "},
					{5,"'(' esperado, recebido "},
					{6,"'fimse', 'leia', 'escreva', 'id' ou 'se' esperado, recebido "},
					{7,"'fim', 'leia', 'escreva', 'id' ou 'se' esperado, recebido "},
					{8,"'fim', 'leia', 'escreva', 'id' ou 'se' esperado, recebido "},
					{9,"'fim', 'leia', 'escreva', 'id' ou 'se' esperado, recebido "},
					{11,"';' esperado, recebido "},
					{13,"'inteiro', 'real' ou 'literal' esperado, recebido "},
					{14,"';' esperado, recebido "},
					{16,"'varfim' ou 'id' esperado, recebido "},
					{17,"'num' ou 'id' esperado, recebido "},
					{20,"operador relacional esperado, recebido "},
					{21,"'num' ou 'id' esperado, recebido "},
					{23,"')' esperado, recebido "},
					{24,"'entao' esperado, recebido "},
					{26," '<-' esperado, recebido "},
					{27,"'num' ou 'id' esperado, recebido "},
					{28,"';' esperado, recebido "},
					{30,"operador matemático esperado, recebido "},
					{31,"'num' ou 'id' esperado, recebido "},
					{33,"'num', 'id' ou 'literal' esperado, recebido "},
					{37,"';' esperado, recebido "},
					{39,"'id' esperado, recebido "},
					{40,"';' esperado, recebido "},
					{44,"'fimse', 'leia', 'escreva' ou 'id' esperado, recebido "},
					{45,"'fimse', 'leia', 'escreva' ou 'se' esperado, recebido "},
					{46,"'fimse' ou 'se' esperado, recebido "},
					{47,"'fimse' ou 'se' esperado, recebido "},
			};
		public:
			const static std::string error(const size_t& state){
				for(auto& i: _error){
					if ( i.first == state ) return i.second;
				}
				return "Erro sintático";
			}
		};
}
