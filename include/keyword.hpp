#pragma once
#include <string>

using std::string;

namespace COM1{
	class keyword {
	public:
		constexpr static const char *keywords[]
				= {"inicio", "varinicio", "varfim", "leia", "escreva",
					"se", "entao", "fimse", "fim", "inteiro", "literal", "real"};


		static bool isKeyword(const std::string &str) {
			for (auto &word: keywords) if (word == str) return true;
			return false;
		}
	};
}
