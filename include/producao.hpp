
#pragma once
#include<vector>
#include<iostream>
#include "alias.hpp"
	namespace COM1::SIN {
		class producao {
			std::string ladoesquerdo;
			std::vector<std::string> ladodireito;
		public:
			std::string toString() const {
				std::string ret;
				ret += ladoesquerdo;
				ret += " → ";
				for (auto word: ladodireito)
					ret += word += " ";
				return ret;
			}

			void print() const { std::cout << toString() << std::endl; }

			producao(std::initializer_list<std::string> words) {
				ladoesquerdo = *(words.begin());
				for (auto i = words.begin() + 1; i != words.end(); ++i)
					ladodireito.push_back(*i);
			}

			size_t comprimentoBeta() const { return ladodireito.size(); }

			std::string left() const {
				return ladoesquerdo;
			}
		};
	}
