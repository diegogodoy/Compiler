#pragma once

#include <sstream>

#include "atributos.hpp"
#include "alias.hpp"
#include "keyword.hpp"
#include "lexer_automata.hpp"

namespace COM1{
	class token {
		std::string name;
		state s;
	public:
		atributosClass atributos;

		token(state s, const std::string &lexema) : s(s) {
			if( s == 19 ){ name  = lexema; return; }
			else {
				atributos.setAtributo("lexema", lexema);
				if (keyword::isKeyword(lexema)) name = std::string("keyword");
				else name = LEX::AFD::getTokenName(s);
				if (s == 6 || s == 3) atributos.setAtributo("tipo", "real");
				if (s == 1) atributos.setAtributo("tipo", "inteiro");
			}
		}

		std::string toString() {
			std::stringstream ss;
			ss << "Q" << s << "\t\t" << atributos.get().at("lexema") << "\t\t" << name;
			return ss.str();
		}

		std::string toStringWithAttributes() const {
			std::stringstream ss;
			ss << "Q" << s << "\t\t" << name << "\t\t{ address:" << this << ", ";
			for(const auto& i : atributos.get())
				ss << i.first << ":" << i.second << ", ";
			ss << "}";
			return ss.str();

		}

		void print() {
			std::cout << toString() << std::endl;
		}

		std::string getLexema() { return atributos.get().at("lexema"); }

		state getState() { return s; }

		std::string getName() { return name; }

	};
}
