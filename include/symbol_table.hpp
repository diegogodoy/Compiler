#pragma once

#include <unordered_set>
#include <string>

#include "alias.hpp"
#include "token.hpp"
#include "keyword.hpp"

namespace COM1{
	class ST { //symbol table
		//std::vector<token> table;
		struct hash {
			std::size_t operator()(ptr<token> a) const {
				std::hash<std::string> hashfn;
				return hashfn(a->getLexema());
			}
		};
		struct pred {
			bool operator()(ptr<token> a, ptr<token> b) const {
				return a->getLexema() == b->getLexema();
			}
		};
		std::unordered_set<ptr<token>, hash, pred> table;

	public:
		ST() {
			for (auto &k : keyword::keywords) {
				table.emplace( std::make_shared<token>(9,k));
				//table.emplace(new token(9,k));
			}
		}

		auto& getTable() const {
			return table;
		}
		ptr<token> insertToken(token&& tk){

			return *table.emplace( std::make_shared<token>(tk) ).first;
		}

		void print() const {
			for(auto& tk : ST::table) {
				tk->print();
			}
		}

		void printWithAttributes() const {
			for(auto& tk : ST::table) {
				std::cerr << tk->toStringWithAttributes() << std::endl;
			}
		}
	};
}
