#pragma once
#include <optional>
#include <unordered_map>
#include "alias.hpp"

namespace COM1{

	class atributosClass{
		std::unordered_map<std::string, std::string> atributos;
	public:
		void setAtributo(const std::string& atributename, std::string value){
			atributos.insert_or_assign(atributename, value);
		}
		opt<std::string> getAtributo(const std::string& atributeName){
			try {
				return { atributos.at(atributeName) };
			}
			catch (std::out_of_range& e){
				return {};
			}
		}
		auto& get() const { return atributos; }
	};
}
