#pragma once
#include <memory>
#include <optional>
#include <utility>

template<class T>
using ptr = std::shared_ptr<T>;
template<class T>
using opt = std::optional<T>;

using state = std::size_t;
