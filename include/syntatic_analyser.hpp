#pragma once
#include <vector>
#include <iostream>

#include "syntatic_automata.hpp"
#include "syntatic_helper.hpp"
#include "lexer.hpp"
#include "gramatica.hpp"

namespace COM1::SIN{
		class SA {
			ST st;
			LA la;
#define DEBUG
#ifdef DEBUG
			std::vector<size_t> e;
			std::vector<ptr<token>> tks;//Referência aos tokens para analise semântica
#else
			std::stack<size_t> e;
#endif
			LR0 lr0;
			gramatica gram;
			size_t tmpVarCounter = 0;
			std::stringstream ssOutput;

			//TODO Remove the following function after debugging
			void printTKS(){
				std::cerr << "\n-------------------------------------------------------------------------------\n";
				for(const auto& i: tks){ std::cerr << i->toStringWithAttributes() << "\n"; }
				std::cerr << "-------------------------------------------------------------------------------\n";
			}
		public:
			SA(const std::string &filename):st(ST()), la(LA(filename, st)){}

#ifdef DEBUG
			void analise() {
				auto ip = la.getNextThreaded();
				e.push_back(0); //Definindo estado inicial do automato
				for (auto i = 0;; ++i) {
					std::cerr << "iteração " << i << std::endl;
					for(const auto& i: e){ std::cerr << i << ", "; }
					std::cerr << "\n";
					auto s = e.back();
					std::cerr << "s <- " << s << "\n";
					size_t a;

					if (auto a_tmp = transitionNumber(*ip)) a = a_tmp.value();
					else throw std::runtime_error("transição inválida)");

					std::cerr << "a <- " << ip->getName() << " ( " << a << " )( " << ip->getLexema() << " )\n";
					const auto transition = lr0.at(a, s);

					if (transition.getAction() == action::shift) {
						e.push_back(a);
						e.push_back(transition.getState().value()); //transition.getState().value() => s'
						tks.push_back(ip);
						ip = la.getNextThreaded();
						std::cerr << "Shift " << transition.getState().value() << "\n\n\n";
					} else if (transition.getAction() == action::reduce) {
						const auto state = transition.getState().value();
						for ( size_t i = 0; i < 2 * gram.at(state).comprimentoBeta(); ++i) { e.pop_back(); }
						const auto s1 = e.back();
						std::cerr << "Reduce " << state << "\n";
						std::cerr << "Popped " << 2*gram.at(state).comprimentoBeta() << " -> ";
						for(const auto& i: e){ std::cerr << i << ", "; }
						std::cerr << "\ns' <- " << s1 << "\n";
						const auto A = gram.A(state);
						std::cerr << "A <- " << A << "\n";
						e.push_back(A); //Empilhando A
						const auto d = lr0.at(A, s1).getState().value();
						std::cerr << "d <- " << d << "\n";
						e.push_back(d);
						gram.at(state).print();

						//Código de baixa qualidade a seguir:  Inclui XGH & gambiarra;
						switch (state){
							case 4: {
								//LV-> Varfim;
								ssOutput << "\n\n\n";
								for(auto i = 0; i < 2; ++i) tks.pop_back();
								break;
							}
							case 5: {
								//D -> id TIPO ;
								//pseudocode:	auto tmp = TIPO.tipo;
								auto& id = tks.at(tks.size()-3);
								auto& TIPO = tks.at(tks.size()-2);
								auto TIPOtipo = TIPO->atributos.getAtributo("tipo").value();
								id->atributos.setAtributo("tipo", TIPOtipo);
								//auto tmp = TIPO;
								for(auto i = 0; i < 3; ++i) tks.pop_back();
								std::string tipoOutput;
								if(TIPOtipo == "inteiro")
									ssOutput << "int " << id->atributos.getAtributo("lexema").value() << ";\n";
								else if(TIPOtipo == "real")
									ssOutput << "double " << id->atributos.getAtributo("lexema").value() << ";\n";
								else if(TIPOtipo == "literal")
									ssOutput << "char " << id->atributos.getAtributo("lexema").value() << "[256];\n";
								break;
							}
							case 6: {
								//TIPO -> inteiro
								//19 means não terminal
								auto TIPO = std::make_shared<token>(19, "TIPO");
								TIPO->atributos.setAtributo("tipo", "inteiro");
								tks.pop_back();
								tks.push_back(TIPO);
								break;
							}
							case 7: {
								//TIPO -> real
								//19 means não terminal
								auto TIPO = std::make_shared<token>(19, "TIPO");
								TIPO->atributos.setAtributo("tipo", "real");
								tks.pop_back();
								tks.push_back(TIPO);
								break;
							}
							case 8: {
								//TIPO -> literal
								//19 means não terminal
								auto TIPO = std::make_shared<token>(19, "TIPO");
								TIPO->atributos.setAtributo("tipo", "literal");
								tks.pop_back();
								tks.push_back(TIPO);
								break;
							}
							case 10: {
								//ES -> leia ID;
								auto& id = tks.at(tks.size()-2);
								for(auto i = 0; i < 3; ++i) tks.pop_back();
								if(auto optidtipo = id->atributos.getAtributo("tipo")){
									auto idtipo = optidtipo.value();
									auto idlexema = id->atributos.getAtributo("lexema").value();
									if(idtipo == "literal") ssOutput << "scanf(\"%s\", " << idlexema << ");\n";
									else if(idtipo == "inteiro") ssOutput << "scanf(\"%d\", &" << idlexema << ");\n";
									else if(idtipo == "real") ssOutput << "scanf(\"%lf\", &" << idlexema << ");\n";
								} else throw std::runtime_error("Variável não declarada");
								break;
							}
							case 11: {
								//ES -> Escreva ARG;
								auto ARG = tks.at(tks.size()-2);
								std::string ARGlexema = ARG->atributos.getAtributo("lexema").value();
								auto ARGtipo = ARG->atributos.getAtributo("tipo").value();

								if(ARGtipo == "literal") ssOutput << "printf(\"%s\\n\", \"" << ARGlexema << "\");\n";
								else if(ARGtipo == "inteiro") ssOutput << "printf(\"%d\\n\", " << ARGlexema << ");\n";
								else if(ARGtipo == "real") ssOutput << "printf(\"%lf\\n\", " << ARGlexema << ");\n";
								//ssOutput << "printf(\"" << ARGlexema << "\");\n";
								for(auto i = 0; i < 3; ++i) tks.pop_back();
								break;
							}
							case 12: {
								//ARG -> literal
								auto &rhs = tks.back();
								auto ARG = std::make_shared<token>(19, "ARG");
								ARG->atributos = rhs->atributos;
								ARG->atributos.setAtributo("tipo", "literal");
								tks.pop_back();
								tks.push_back(ARG);
								break;
							}
							case 13: {
								//ARG -> num
								auto& rhs = tks.back();
								auto ARG = std::make_shared<token>(19, "ARG");
								ARG->atributos = rhs->atributos;
								ARG->atributos.setAtributo("tipo", "numero");
								tks.pop_back();
								tks.push_back(ARG);
								break;
							}
							case 14: {
								//ARG -> id
								auto id = tks.back();
								if(id->atributos.getAtributo("tipo")) {
									auto ARG = std::make_shared<token>(19, "ARG");
									ARG->atributos = id->atributos;
									tks.pop_back();
									tks.push_back(ARG);
								} else throw std::runtime_error("Variável não declarada");
								break;
							}
							case 16: {
								//CMD -> id rcb LD;
								auto& id = tks.at(tks.size()-4);
								auto& rhs = tks.at(tks.size()-2);
								if(auto optidtipo = id->atributos.getAtributo("tipo")){
									//erro na próxima linha de código se LD foi calculado; ( ex case 17 )
									std::string rhsTIPO = rhs->atributos.getAtributo("tipo").value();
									if(rhsTIPO == optidtipo.value()){
										ssOutput << id->atributos.getAtributo("lexema").value()
													<< " = " << rhs->atributos.getAtributo("lexema").value()
													<< ";\n";
										for(auto i = 0; i < 4; ++i) tks.pop_back();
									} else {
										std::stringstream ss;
										ss << "lhs e rhs não são do mesmo tipo\n";
										ss << "lhs.tipo = " << optidtipo.value() << "; rhs.tipo = " << rhsTIPO << "\n";
										throw std::runtime_error(ss.str());
									}
								} else throw std::runtime_error("Variável não declarada");
								break;
							}
							case 17: {
								//LD -> OPRD opm OPRD
								auto& lhs = tks.at(tks.size()-3);
								auto& rhs = tks.back();
								std::string lhsTIPO = lhs->atributos.getAtributo("tipo").value();
								std::string rhsTIPO = rhs->atributos.getAtributo("tipo").value();
								if(lhsTIPO == rhsTIPO && lhsTIPO != "literal"){
									std::stringstream ss;
									ss << "T" << tmpVarCounter;
									tmpVarCounter++;
									auto TxPtr = st.insertToken(token(20, ss.str()));
									auto LD = std::make_shared<token>(19, "LD");

									TxPtr->atributos.setAtributo("tipo", lhsTIPO);
									LD->atributos.setAtributo("tipo", lhsTIPO);
									LD->atributos.setAtributo("lexema", ss.str()); //LD.lexema = Tx

									auto opm = tks.at(tks.size()-2);
									ssOutput << ss.str() << " = "
											<< lhs->atributos.getAtributo("lexema").value() << " "
											<< opm->atributos.getAtributo("lexema").value() << " "
											<< rhs->atributos.getAtributo("lexema").value() << ";\n";
									for(auto i = 0; i < 3; ++i) tks.pop_back();
									tks.push_back(LD);
								} else throw std::runtime_error("lhs e rhs não são do mesmo tipo\n");
								printTKS();
								break;
							}
							case 18: {
								//LD -> OPRD
								auto LD = std::make_shared<token>(19, "LD");
								LD->atributos = tks.back()->atributos;
								tks.pop_back();
								tks.push_back(LD);
								break;
							}
							case 19: {
								//OPRD-> id
								if(auto optidtipo = tks.back()->atributos.getAtributo("tipo")){
									auto OPRD = std::make_shared<token>(19, "OPRD");
									OPRD->atributos = tks.back()->atributos;
									tks.pop_back();
									tks.push_back(OPRD);
								} else throw std::runtime_error("Variável não declarada");
								break;
							}
							case 20: {
								//OPRD-> num
								auto OPRD = std::make_shared<token>(19, "OPRD");
								OPRD->atributos = tks.back()->atributos;
								tks.pop_back();
								tks.push_back(OPRD);
								break;
							}
							case 22: {
								//COND -> cabecalho corpo
								ssOutput << "}\n";
								break;
							}
							case 23: {
								//cabecalho -> se ABP EXPR FCP entao
								auto& EXPR = tks.at(tks.size()-3);
								ssOutput << "if ( " << EXPR->atributos.getAtributo("lexema").value() << " ) {\n";
								for(auto i = 0; i < 5; ++i) tks.pop_back();
								break;
							}
							case 24: {
								//EXPR -> OPRD opr OPRD
								auto& lhs = tks.at(tks.size()-3);
								auto& rhs = tks.back();
								std::string lhsTIPO = lhs->atributos.getAtributo("tipo").value();
								std::string rhsTIPO = rhs->atributos.getAtributo("tipo").value();
								if(lhsTIPO == rhsTIPO && lhsTIPO != "literal"){
									std::stringstream ss;
									ss << "T" << tmpVarCounter;
									tmpVarCounter++;
									auto TxPtr = st.insertToken(token(20, ss.str()));
									auto EXPR = std::make_shared<token>(19, "EXPR");

									TxPtr->atributos.setAtributo("tipo", "boleano");
									EXPR->atributos.setAtributo("tipo", "boleano");
									EXPR->atributos.setAtributo("lexema", ss.str()); //LD.lexema = Tx

									auto opr = tks.at(tks.size()-2);
									ssOutput << ss.str() << " = "
											<< lhs->atributos.getAtributo("lexema").value() << " "
											<< opr->atributos.getAtributo("lexema").value() << " "
											<< rhs->atributos.getAtributo("lexema").value() << ";\n";
									for(auto i = 0; i < 3; ++i) tks.pop_back();
									tks.push_back(EXPR);
								} else throw std::runtime_error("lhs e rhs não são do mesmo tipo\n");
								break;
							}
						}
					} else if ( transition.getAction() == action::accept) {
						std::cout << "Analise Sintática concluída com sucesso\n";
						return ;
					} else {
						std::stringstream ss; ss << "Invalid Action at line " << __LINE__;
						throw std::runtime_error(ss.str());
					}
				}
			}
#else
			void analise() {
				//auto ip = la.getNext();
				auto ip = la.getNextThreaded();
				e.push(0); //Definindo estado inicial do automato
				for (;;) {
					auto s = e.top();
					size_t a;

					if (auto a_tmp = transitionNumber(ip)) a = a_tmp.value();
					else throw std::runtime_error("transição inválida // Token Desconhecido");

					const auto transition = lr0.at(a, s);

					if (transition.getAction() == action::shift) {
						e.push(a);
						e.push(transition.getState().value()); //transition.getState().value() => s'
						ip = la.getNextThreaded();
					} else if (transition.getAction() == action::reduce) {
						const auto prodNumber = transition.getState().value();
						for ( size_t i = 0; i < 2 * gram.at(prodNumber).comprimentoBeta(); ++i) { e.pop(); }
						const auto s1 = e.top();
						const auto A = gram.A(prodNumber);
						const auto d = lr0.at(A, s1).getState().value();

						e.push(A);
						e.push(d);
						gram.at(prodNumber).print();
						if(prodNumber == 6) {
							std::cout << "\t\tRedução TIPO -> INTEIRO\n";
							std::cout << ip.toString() << std::endl;
						}

					} else if ( transition.getAction() == action::accept ) {
						std::cout << "Analise Sintática concluída com sucesso\n";
						return ;
					} else if (transition.getAction() == action::error ) {
						std::stringstream ss;
						ss << "\n" << sintaticError::error(s);
						ss << "'"<< ip.getLexema() << "'\n";
						auto pos = la.pos();
						ss << "Linha: " << std::get<0>(pos) << "\tColuna: " << std::get<1>(pos) << std::endl;
						ss << "Token:\t";
						ss << ip.toString();
						throw std::runtime_error(ss.str());
					}
				}
			}
#endif
			std::string stringOut(){ //This should be called only once on lifetime of this object and after analise.
				std::string ret;
				ret += "#include <stdio.h>\n\n";
				ret += "int main(){\n";
				for(auto& i : st.getTable()){
					if ( i->getName() == "TMP VAR" ){
						auto tipo = i->atributos.getAtributo("tipo").value();
						if ( tipo == "boleano" ) ret += "int ";
						else if  ( tipo == "inteiro") ret += "int ";
						else if  ( tipo == "real") ret += "double ";
						else throw std::runtime_error("linha __LINE__");
						ret += i->atributos.getAtributo("lexema").value();
						ret += " ;\n";
					}
				}
				ret += ssOutput.str();
				ret += "\nreturn 0;\n}\n";
				return ret;
			}
		};
}

