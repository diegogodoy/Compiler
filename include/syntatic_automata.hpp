#pragma once
#include "alias.hpp"
#include "syntatic_helper.hpp"

namespace COM1::SIN{

		class LR0 { //Não irá armazenar estado atual, pois isso será salvo no stack no analisador sintático
			transitionTableItem<action> table[59][37];

			void initReduce(state s, state reduce){
				for(auto i = 0; i < 22; ++i)
					table[s][i].set(action::reduce, reduce);
			}
			void init() {
				for (auto &i : table)
					for (auto &j : i)
						j.set();
				//S0
				table[0][0].set(action::shift, 2);
				table[0][22].set(action::none, 1);
				//S1
				table[1][21].set(action::accept, 0); //botei S0 pq sim, Undefined behavior
				//S2
				table[2][1].set(action::shift, 3);
				table[2][23].set(action::none, 4);
				//S3
				table[3][2].set(action::shift, 11);
				table[3][4].set(action::shift, 13);
				table[3][25].set(action::none, 10);
				table[3][26].set(action::none, 16);
				//S4
				table[4][4].set(action::shift, 26);
				table[4][8].set(action::shift, 39);
				table[4][9].set(action::shift, 33);
				table[4][14].set(action::shift, 5);
				table[4][20].set(action::shift, 42);
				table[4][24].set(action::none, 51);
				table[4][28].set(action::none, 9);
				table[4][30].set(action::none, 8);
				table[4][33].set(action::none, 7);
				table[4][34].set(action::none, 6);
				//S5
				table[5][15].set(action::shift, 17);
				//S6
				table[6][4].set(action::shift, 26);
				table[6][8].set(action::shift, 39);
				table[6][9].set(action::shift, 33);
				table[6][14].set(action::shift, 5);
				table[6][19].set(action::shift, 43);
				table[6][28].set(action::none, 46);
				table[6][30].set(action::none, 45);
				table[6][33].set(action::none, 44);
				table[6][34].set(action::none, 6);
				table[6][35].set(action::none, 47);
				//S7
				table[7][4].set(action::shift, 26);
				table[7][8].set(action::shift, 39);
				table[7][9].set(action::shift, 33);
				table[7][14].set(action::shift, 5);
				table[7][20].set(action::shift, 42);
				table[7][24].set(action::none, 54);
				table[7][28].set(action::none, 9);
				table[7][30].set(action::none, 8);
				table[7][33].set(action::none, 7);
				table[7][34].set(action::none, 6);
				//S8
				table[8][4].set(action::shift, 26);
				table[8][8].set(action::shift, 39);
				table[8][9].set(action::shift, 33);
				table[8][14].set(action::shift, 5);
				table[8][20].set(action::shift, 42);
				table[8][24].set(action::none, 53);
				table[8][28].set(action::none, 9);
				table[8][30].set(action::none, 8);
				table[8][33].set(action::none, 7);
				table[8][34].set(action::none, 6);
				//S9
				table[9][4].set(action::shift, 26);
				table[9][8].set(action::shift, 39);
				table[9][9].set(action::shift, 33);
				table[9][14].set(action::shift, 5);
				table[9][20].set(action::shift, 42);
				table[9][24].set(action::none, 52);
				table[9][28].set(action::none, 9);
				table[9][30].set(action::none, 8);
				table[9][33].set(action::none, 7);
				table[9][34].set(action::none, 6);
				//S10
				initReduce(10, 2);
				//S11
				table[11][3].set(action::shift, 12);
				//S12
				initReduce(12, 4);
				//S13
				table[13][5].set(action::shift, 56);
				table[13][6].set(action::shift, 57);
				table[13][7].set(action::shift, 58);
				table[13][27].set(action::none, 14);
				//S14
				table[14][3].set(action::shift, 15);
				//S15
				initReduce(15, 5);
				//S16
				table[16][2].set(action::shift, 11);
				table[16][4].set(action::shift, 13);
				table[16][25].set(action::none, 55);
				table[16][26].set(action::none, 16);
				//S17
				table[17][4].set(action::shift, 18);
				table[17][11].set(action::shift, 19);
				table[17][32].set(action::shift, 20);
				table[17][36].set(action::shift, 23);
				//S18
				initReduce(18, 19);
				//S19
				initReduce(19, 20);
				//S20
				table[20][18].set(action::shift, 21);
				//S21
				table[21][4].set(action::shift, 18);
				table[21][11].set(action::shift, 19);
				table[21][32].set(action::none, 22);
				//S22
				initReduce(22, 24);
				//S23
				table[23][16].set(action::shift, 24);
				//S24
				table[24][17].set(action::shift, 25);
				//S25
				initReduce(25, 23);
				//S26
				table[26][12].set(action::shift, 27);
				//S27
				table[27][4].set(action::shift, 18);
				table[27][11].set(action::shift, 19);
				table[27][31].set(action::none, 28);
				table[27][32].set(action::none, 30);
				//S28
				table[28][3].set(action::shift, 29);
				//S29
				initReduce(29,16);
				//S30
				table[30][3].set(action::reduce, 18);
				table[30][13].set(action::shift, 31);
				//S31
				table[31][4].set(action::shift, 18);
				table[31][11].set(action::shift, 19);
				table[31][32].set(action::none, 32);
				//S32
				initReduce(32, 17);
				//S33
				table[33][4].set(action::shift, 36);
				table[33][10].set(action::shift, 34);
				table[33][11].set(action::shift, 35);
				table[33][29].set(action::none, 37);
				//S34
				initReduce(34, 12);
				//S35
				initReduce(35, 13);
				//S36
				initReduce(36, 14);
				//S37
				table[37][3].set(action::shift, 38);
				//S38
				initReduce(38, 11);
				//S39
				table[39][4].set(action::shift, 40);
				//S40
				table[40][3].set(action::shift, 41);
				//S41
				initReduce(41, 10);
				//S42
				initReduce(42, 29);
				//S43
				initReduce(43, 28);
				//S44
				table[44][4].set(action::shift, 26);
				table[44][8].set(action::shift, 39);
				table[44][9].set(action::shift, 33);
				table[44][19].set(action::shift, 43);
				table[44][28].set(action::none, 46);
				table[44][30].set(action::none, 45);
				table[44][35].set(action::none, 49);
				//S45
				table[45][8].set(action::shift, 39);
				table[45][9].set(action::shift, 33);
				table[45][14].set(action::shift, 5);
				table[45][19].set(action::shift, 43);
				table[45][28].set(action::none, 46);
				table[45][33].set(action::none, 44);
				table[45][34].set(action::none, 6);
				table[45][35].set(action::none, 50);
				//S46
				table[46][4].set(action::shift, 26);
				table[46][14].set(action::shift, 5);
				table[46][19].set(action::shift, 43);
				table[46][30].set(action::none, 45);
				table[46][33].set(action::none, 44);
				table[46][34].set(action::none, 6);
				table[46][35].set(action::none, 48);
				//S47
				initReduce(47, 22);
				//S48
				initReduce(48, 25);
				//S49
				initReduce(49, 27);
				//S50
				initReduce(50, 26);
				//S51
				initReduce(51, 1);
				//S52
				initReduce(52, 9);
				//S53
				initReduce(53, 15);
				//S54
				initReduce(54, 21);
				//S55
				initReduce(55, 3);
				//S56
				initReduce(56, 6);
				//S57
				initReduce(57, 7);
				//S58
				initReduce(58, 8);
			}

		public:
			LR0() { init(); }

			const transitionTableItem<action> &
			at(size_t transition, size_t state) const { return table[state][transition]; }
		};
}
