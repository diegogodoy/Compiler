#pragma once
#include "producao.hpp"
#include "syntatic_helper.hpp"
namespace COM1::SIN{
		//TODO gramatica full static?
		class gramatica {
			std::vector<producao> data;
		public:
			gramatica() {
				data.push_back({"P'", "P"});
				data.push_back({"P", "inicio", "V", "A"});
				data.push_back({"V", "varinicio", "LV"});
				data.push_back({"LV", "D", "LV"});
				data.push_back({"LV", "varfim", "PTV"});
				data.push_back({"D", "id", "TIPO", "PTV"});
				data.push_back({"TIPO", "int"});
				data.push_back({"TIPO", "real"});
				data.push_back({"TIPO", "lit"});
				data.push_back({"A", "ES", "A"});
				data.push_back({"ES", "leia", "id", "PTV"});
				data.push_back({"ES", "escreva", "ARG", "PTV"});
				data.push_back({"ARG", "literal"});
				data.push_back({"ARG", "num"});
				data.push_back({"ARG", "id"});
				data.push_back({"A", "CMD", "A"});
				data.push_back({"CMD", "id", "rcb", "LD", "PTV"});
				data.push_back({"LD", "OPRD", "opm", "OPRD"});
				data.push_back({"LD", "OPRD",});
				data.push_back({"OPRD", "id"});
				data.push_back({"OPRD", "num"});
				data.push_back({"A", "COND", "A"});
				data.push_back({"COND", "CABECALHO", "CORPO"});
				data.push_back({"CABECALHO", "se", "ABP", "EXPR", "FCP", "entao"});
				data.push_back({"EXPR", "OPRD", "opr", "OPRD"});
				data.push_back({"CORPO", "ES", "CORPO"});
				data.push_back({"CORPO", "CMD", "CORPO"});
				data.push_back({"CORPO", "COND", "CORPO"});
				data.push_back({"CORPO", "fimse"});
				data.push_back({"A", "fim"});
			}

			void print() const {
				for (size_t i = 0; i < data.size(); ++i)
					std::cout << i << "\t" << data[i].toString() << std::endl;
			}

			const producao& at(size_t pos) const { return data.at(pos); }

			size_t A(size_t prodNumber) { //(enter prod number, return A code)
				const std::string left = this->at(prodNumber).left();
				if(left == "P\'") return 37;
				if(left == "P") return 22;
				if(left == "V") return 23;
				if(left == "A") return 24;
				if(left == "LV") return 25;
				if(left == "D") return 26;
				if(left == "TIPO") return 27;
				if(left == "ES") return 28;
				if(left == "ARG") return 29;
				if(left == "CMD") return 30;
				if(left == "LD") return 31;
				if(left == "OPRD") return 32;
				if(left == "COND") return 33;
				if(left == "CABECALHO") return 34;
				if(left == "CORPO") return 35;
				if(left == "EXPR") return 36;
				throw std::runtime_error("Erro: Lado Esquerdo não existe na gramática");
			}
		};
}
