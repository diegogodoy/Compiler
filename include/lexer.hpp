#pragma once

#include <thread>
#include <mutex>
#include <fstream>

#include "alias.hpp"
#include "lexer_automata.hpp"
#include "symbol_table.hpp"
namespace COM1{
	class LA {

		std::string buffer;
		LEX::AFD afd;
		std::string input;
		std::size_t indexOfInputFile = 0;
		std::size_t lineOfFile = 1;
		std::size_t colOfFile = 0;
		ST &st;

		ptr<token> threadBuffer;
		std::mutex mThreadBufferMutex;
		std::thread t;

		static std::string fileToString(std::string &filename) {
			std::ifstream file(filename.c_str());
			std::stringstream ss;
			ss << file.rdbuf();
			auto str = ss.str();
			str += '$';// <- EOF!
			return str;
		}

	public:

		LA(std::string filename, ST &st) : input(fileToString(filename)), st(st), t(&LA::producer, this) {};
		//LA(std::string filename, ST &st) : input(fileToString(filename)), st(st) {};

		ptr<token> getNext() {
			auto ret = std::string();
			try {
				for (auto &i = indexOfInputFile; i < input.size(); ++i) {
					colOfFile++;
					auto &c = input.at(i);
					if (c == '\n' || c == '\r') {
						lineOfFile++;
						colOfFile = 0;
					}
					if (c == '$') { return st.insertToken(token(12, "EOF")); }
					auto prevState = afd.getState();
					auto newState = afd.transition(c);
					auto action = newState.getAction();

					//switch(action)
					if (action == LEX::outAlphabet::append) {
						buffer += c;
						continue;
					}
					if (action == LEX::outAlphabet::clear) {
						buffer.clear();
						continue;
					}
					if (action == LEX::outAlphabet::returm) {
						ret = buffer;
						buffer.clear();
						i++;
						return st.insertToken(token(prevState, ret));
					}
					if (action == LEX::outAlphabet::returnAppend) {
						ret = buffer;
						buffer.clear();
						buffer += c;
						i++;
						return st.insertToken(token(prevState, ret));
					}
					if (action == LEX::outAlphabet::ignore) {
						continue;
					}
					if (action == LEX::outAlphabet::error) {
						std::stringstream s;
						s << "Invalid Charactere:\t" << c << std::endl;
						s << "Actual State:\t" << prevState << std::endl;
						s << "Buffer Content:\t" << buffer << std::endl;
						s << "Error in source code Line:\n" << lineOfFile << "\tColuna:\t" << colOfFile;
						throw std::runtime_error(s.str());
					}
					throw std::runtime_error(std::to_string(lineOfFile));
				}
			} catch (std::runtime_error &e) {
				throw std::runtime_error(e.what());
			}
			throw std::runtime_error("getToken() não conseguiu gerar um token");
		}

		std::tuple<size_t, size_t> pos() const { return { this->lineOfFile, this->colOfFile }; }

		void producer() { //Producer
			while (true) {
				std::scoped_lock lock{mThreadBufferMutex};
				if (not threadBuffer) threadBuffer = getNext();
			}
		}
//TODO test threaded version but using atomic buffer instead of mutexes, compare performance
	//Is the sugested approach viable with pointers?
		ptr<token> getNextThreaded(){ //Consumer
			while(true){
				std::scoped_lock lock{mThreadBufferMutex};
				if(threadBuffer) {
					//auto tmp = threadBuffer.value();
					//threadBuffer = std::nullopt;
					auto tmp = threadBuffer;
					threadBuffer = nullptr;
					return tmp;
				}
			}

		}

		~LA(){
			t.detach();
			t.~thread();
		}
	};
}
