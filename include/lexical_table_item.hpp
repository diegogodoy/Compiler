#include <utility>
#include <optional>
#include <iostream>

#include "alias.hpp"


namespace COM1{

template<class A> //A requires ENUM CLASS e 'A::error'
class transitionTableItem : public std::pair<opt<state>, A> {
	// Se e somente se action = E, então State = nullopt
public:
	void set() {
		this->first = std::nullopt;
		this->second = A::error; //this is gambiarra, error should always be the first element in the enum
	}

	void set(A action, state s) {
		if (action == A::error) std::cerr << "Error setting table. use overloaded function\n";
		this->first = s;
		//std::pair<opt<state>, outAlphabet>.first = s;
		this->second = action;
	}

	opt<state> getState() const { return this->first; }

	A getAction() const { return this->second; }
};
}
