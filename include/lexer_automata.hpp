#pragma once
#include <sstream>
#include "alias.hpp"
#include "lexical_table_item.hpp"

namespace COM1::LEX {
		enum class outAlphabet {
			error,
			clear,
			append,
			ignore,
			returnAppend,
			returm,
			//"return" é keyword
					C = clear,
			A = append,
			I = ignore,
			RA = returnAppend,
			R = returm,
			E = error
		};

		opt<size_t> transitionNumber(char c) {

			if (c == 'E') return 18;
			if (isspace(c)) return 0;
			if (isalpha(c)) return 1;
			if (isdigit(c)) return 2;
			if (c == '\"') return 3;
			if (c == '_') return 4;
			if (c == '{') return 5;
			if (c == '}') return 6;
			if (c == '(') return 7;
			if (c == ')') return 8;
			if (c == '<') return 9;
			if (c == '>') return 10;
			if (c == '=') return 11;
			if (c == '+') return 12;
			if (c == '-') return 13;
			if (c == '*') return 14;
			if (c == '/') return 15;
			if (c == ';') return 16;
			if (c == '$') return 17;
			if (c == '.') return 19;
			if (c == '\\') return 20;
			if (c == ':') return 21;
			return std::nullopt;
		}

		class AFD {
			transitionTableItem<outAlphabet> transitionTable[19][22];
			//[state][transition]

			void initializeTable() {
				for (auto &i : AFD::transitionTable)
					for (auto &j: i)
						j.set();
				//Q0
				transitionTable[0][0].set(outAlphabet::I, 0);
				transitionTable[0][1].set(outAlphabet::A, 9);
				transitionTable[0][2].set(outAlphabet::A, 1);
				transitionTable[0][3].set(outAlphabet::I, 7);
				transitionTable[0][4].set(outAlphabet::A, 9);
				transitionTable[0][5].set(outAlphabet::I, 10);
				transitionTable[0][7].set(outAlphabet::A, 18);
				transitionTable[0][8].set(outAlphabet::A, 8);
				transitionTable[0][9].set(outAlphabet::A, 13);
				transitionTable[0][10].set(outAlphabet::A, 16);
				transitionTable[0][11].set(outAlphabet::A, 14);
				transitionTable[0][12].set(outAlphabet::A, 17);
				transitionTable[0][13].set(outAlphabet::A, 17);
				transitionTable[0][14].set(outAlphabet::A, 17);
				transitionTable[0][15].set(outAlphabet::A, 17);
				transitionTable[0][16].set(outAlphabet::A, 11);
				transitionTable[0][17].set(outAlphabet::I, 12);
				transitionTable[0][18].set(outAlphabet::A, 9);

				//Q1
				transitionTable[1][0].set(outAlphabet::R, 0);
				transitionTable[1][2].set(outAlphabet::A, 1);
				transitionTable[1][5].set(outAlphabet::R, 10);
				transitionTable[1][7].set(outAlphabet::RA, 18);
				transitionTable[1][8].set(outAlphabet::RA, 8);
				transitionTable[1][9].set(outAlphabet::RA, 13);
				transitionTable[1][10].set(outAlphabet::RA, 16);
				transitionTable[1][11].set(outAlphabet::RA, 14);
				transitionTable[1][12].set(outAlphabet::RA, 17);
				transitionTable[1][13].set(outAlphabet::RA, 17);
				transitionTable[1][14].set(outAlphabet::RA, 17);
				transitionTable[1][15].set(outAlphabet::RA, 17);
				transitionTable[1][16].set(outAlphabet::RA, 11);
				transitionTable[1][17].set(outAlphabet::R, 12);
				transitionTable[1][18].set(outAlphabet::A, 4);
				transitionTable[1][19].set(outAlphabet::A, 2);

				//Q2
				transitionTable[2][2].set(outAlphabet::A, 3);

				//Q3
				transitionTable[3][0].set(outAlphabet::R, 0);
				transitionTable[3][2].set(outAlphabet::A, 3);
				transitionTable[3][8].set(outAlphabet::RA, 8);
				transitionTable[3][9].set(outAlphabet::RA, 13);
				transitionTable[3][10].set(outAlphabet::RA, 16);
				transitionTable[3][11].set(outAlphabet::RA, 14);
				transitionTable[3][12].set(outAlphabet::RA, 17);
				transitionTable[3][13].set(outAlphabet::RA, 17);
				transitionTable[3][14].set(outAlphabet::RA, 17);
				transitionTable[3][15].set(outAlphabet::RA, 17);
				transitionTable[3][16].set(outAlphabet::RA, 11);
				transitionTable[3][17].set(outAlphabet::R, 12);
				transitionTable[3][18].set(outAlphabet::A, 4);

				//Q4
				transitionTable[4][2].set(outAlphabet::A, 6);
				transitionTable[4][12].set(outAlphabet::A, 5);
				transitionTable[4][13].set(outAlphabet::A, 5);

				//Q5
				transitionTable[5][2].set(outAlphabet::A, 6);

				//Q6
				transitionTable[6][0].set(outAlphabet::R, 0);
				transitionTable[6][2].set(outAlphabet::A, 6);
				transitionTable[6][5].set(outAlphabet::R, 10);
				transitionTable[6][7].set(outAlphabet::RA, 18);
				transitionTable[6][8].set(outAlphabet::RA, 8);
				transitionTable[6][9].set(outAlphabet::RA, 13);
				transitionTable[6][10].set(outAlphabet::RA, 16);
				transitionTable[6][11].set(outAlphabet::RA, 14);
				transitionTable[6][12].set(outAlphabet::RA, 17);
				transitionTable[6][13].set(outAlphabet::RA, 17);
				transitionTable[6][14].set(outAlphabet::RA, 17);
				transitionTable[6][15].set(outAlphabet::RA, 17);
				transitionTable[6][16].set(outAlphabet::RA, 11);
				transitionTable[6][17].set(outAlphabet::R, 12);

				//Q7
				transitionTable[7][0].set(outAlphabet::A, 7);
				transitionTable[7][1].set(outAlphabet::A, 7);
				transitionTable[7][2].set(outAlphabet::A, 7);
				transitionTable[7][3].set(outAlphabet::R, 0);
				transitionTable[7][4].set(outAlphabet::A, 7);
				transitionTable[7][5].set(outAlphabet::A, 7);
				transitionTable[7][6].set(outAlphabet::A, 7);
				transitionTable[7][7].set(outAlphabet::A, 7);
				transitionTable[7][8].set(outAlphabet::A, 7);
				transitionTable[7][9].set(outAlphabet::A, 7);
				transitionTable[7][10].set(outAlphabet::A, 7);
				transitionTable[7][11].set(outAlphabet::A, 7);
				transitionTable[7][12].set(outAlphabet::A, 7);
				transitionTable[7][13].set(outAlphabet::A, 7);
				transitionTable[7][14].set(outAlphabet::A, 7);
				transitionTable[7][15].set(outAlphabet::A, 7);
				transitionTable[7][16].set(outAlphabet::A, 7);
				transitionTable[7][17].set(outAlphabet::I, 12);
				transitionTable[7][18].set(outAlphabet::A, 7);
				transitionTable[7][19].set(outAlphabet::A, 7);
				transitionTable[7][20].set(outAlphabet::A, 7);
				transitionTable[7][21].set(outAlphabet::A, 7);

				//Q8
				transitionTable[8][0].set(outAlphabet::R, 0);
				transitionTable[8][1].set(outAlphabet::RA, 9);
				transitionTable[8][2].set(outAlphabet::RA, 1);
				transitionTable[8][5].set(outAlphabet::R, 10);
				transitionTable[8][16].set(outAlphabet::RA, 11);
				transitionTable[8][17].set(outAlphabet::R, 12);
				transitionTable[8][18].set(outAlphabet::RA, 9);
				transitionTable[8][19].set(outAlphabet::RA, 18);

				//Q9
				transitionTable[9][0].set(outAlphabet::R, 0);
				transitionTable[9][1].set(outAlphabet::A, 9);
				transitionTable[9][2].set(outAlphabet::A, 9);
				transitionTable[9][3].set(outAlphabet::RA, 7);
				transitionTable[9][4].set(outAlphabet::A, 9);
				transitionTable[9][5].set(outAlphabet::R, 10);
				transitionTable[9][7].set(outAlphabet::RA, 18);
				transitionTable[9][8].set(outAlphabet::RA, 8);
				transitionTable[9][9].set(outAlphabet::RA, 13);
				transitionTable[9][10].set(outAlphabet::RA, 16);
				transitionTable[9][11].set(outAlphabet::RA, 14);
				transitionTable[9][12].set(outAlphabet::RA, 17);
				transitionTable[9][13].set(outAlphabet::RA, 17);
				transitionTable[9][14].set(outAlphabet::RA, 17);
				transitionTable[9][15].set(outAlphabet::RA, 17);
				transitionTable[9][16].set(outAlphabet::RA, 11);
				transitionTable[9][17].set(outAlphabet::R, 12);
				transitionTable[9][18].set(outAlphabet::A, 9);

				//Q10
				transitionTable[10][0].set(outAlphabet::I, 10);
				transitionTable[10][1].set(outAlphabet::I, 10);
				transitionTable[10][2].set(outAlphabet::I, 10);
				transitionTable[10][3].set(outAlphabet::I, 10);
				transitionTable[10][4].set(outAlphabet::I, 10);
				transitionTable[10][5].set(outAlphabet::I, 10);
				transitionTable[10][6].set(outAlphabet::I, 0);
				transitionTable[10][7].set(outAlphabet::I, 10);
				transitionTable[10][8].set(outAlphabet::I, 10);
				transitionTable[10][9].set(outAlphabet::I, 10);
				transitionTable[10][10].set(outAlphabet::I, 10);
				transitionTable[10][11].set(outAlphabet::I, 10);
				transitionTable[10][12].set(outAlphabet::I, 10);
				transitionTable[10][13].set(outAlphabet::I, 10);
				transitionTable[10][14].set(outAlphabet::I, 10);
				transitionTable[10][15].set(outAlphabet::I, 10);
				transitionTable[10][16].set(outAlphabet::I, 10);
				transitionTable[10][17].set(outAlphabet::I, 12);
				transitionTable[10][18].set(outAlphabet::I, 10);
				transitionTable[10][19].set(outAlphabet::I, 10);
				transitionTable[10][20].set(outAlphabet::I, 10);
				transitionTable[10][21].set(outAlphabet::I, 10);

				//Q11
				transitionTable[11][0].set(outAlphabet::R, 0);
				transitionTable[11][17].set(outAlphabet::R, 12);

				//Q12//estado calabouço, sem transição possíveis
				transitionTable[10][0].set(outAlphabet::I, 12);
				transitionTable[10][1].set(outAlphabet::I, 12);
				transitionTable[10][2].set(outAlphabet::I, 12);
				transitionTable[10][3].set(outAlphabet::I, 12);
				transitionTable[10][4].set(outAlphabet::I, 12);
				transitionTable[10][5].set(outAlphabet::I, 12);
				transitionTable[10][6].set(outAlphabet::I, 12);
				transitionTable[10][7].set(outAlphabet::I, 12);
				transitionTable[10][8].set(outAlphabet::I, 12);
				transitionTable[10][9].set(outAlphabet::I, 12);
				transitionTable[10][10].set(outAlphabet::I, 12);
				transitionTable[10][11].set(outAlphabet::I, 12);
				transitionTable[10][12].set(outAlphabet::I, 12);
				transitionTable[10][13].set(outAlphabet::I, 12);
				transitionTable[10][14].set(outAlphabet::I, 12);
				transitionTable[10][15].set(outAlphabet::I, 12);
				transitionTable[10][16].set(outAlphabet::I, 12);
				transitionTable[10][17].set(outAlphabet::I, 12);
				transitionTable[10][18].set(outAlphabet::I, 12);
				transitionTable[10][19].set(outAlphabet::I, 12);
				transitionTable[10][20].set(outAlphabet::I, 12);
				transitionTable[10][21].set(outAlphabet::I, 12);

				//Q13
				transitionTable[13][0].set(outAlphabet::R, 0);
				transitionTable[13][1].set(outAlphabet::RA, 9);
				transitionTable[13][2].set(outAlphabet::RA, 1);
				transitionTable[13][7].set(outAlphabet::R, 10);
				transitionTable[13][9].set(outAlphabet::RA, 18);
				transitionTable[13][10].set(outAlphabet::A, 14);
				transitionTable[13][11].set(outAlphabet::A, 14);
				transitionTable[13][13].set(outAlphabet::A, 15);
				transitionTable[13][17].set(outAlphabet::R, 12);
				transitionTable[13][18].set(outAlphabet::RA, 9);

				//Q14
				transitionTable[14][0].set(outAlphabet::R, 0);
				transitionTable[14][1].set(outAlphabet::RA, 9);
				transitionTable[14][2].set(outAlphabet::RA, 1);
				transitionTable[14][9].set(outAlphabet::RA, 18);
				transitionTable[14][10].set(outAlphabet::A, 14);
				transitionTable[14][11].set(outAlphabet::A, 14);
				transitionTable[14][13].set(outAlphabet::A, 15);
				transitionTable[14][17].set(outAlphabet::R, 12);
				transitionTable[14][18].set(outAlphabet::RA, 9);

				//Q15
				transitionTable[15][0].set(outAlphabet::R, 0);
				transitionTable[15][1].set(outAlphabet::RA, 9);
				transitionTable[15][2].set(outAlphabet::RA, 1);
				transitionTable[15][7].set(outAlphabet::R, 10);
				transitionTable[15][9].set(outAlphabet::RA, 18);
				transitionTable[15][17].set(outAlphabet::R, 12);
				transitionTable[15][18].set(outAlphabet::RA, 9);

				//Q16
				transitionTable[16][0].set(outAlphabet::R, 0);
				transitionTable[16][1].set(outAlphabet::RA, 9);
				transitionTable[16][2].set(outAlphabet::RA, 1);
				transitionTable[16][7].set(outAlphabet::R, 10);
				transitionTable[16][9].set(outAlphabet::RA, 18);
				transitionTable[16][11].set(outAlphabet::A, 14);
				transitionTable[16][17].set(outAlphabet::R, 12);
				transitionTable[16][18].set(outAlphabet::RA, 9);

				//Q17
				transitionTable[17][0].set(outAlphabet::R, 0);
				transitionTable[17][1].set(outAlphabet::RA, 9);
				transitionTable[17][2].set(outAlphabet::RA, 1);
				transitionTable[17][7].set(outAlphabet::R, 10);
				transitionTable[17][9].set(outAlphabet::RA, 18);
				transitionTable[17][17].set(outAlphabet::R, 12);
				transitionTable[17][18].set(outAlphabet::RA, 9);

				//Q18
				transitionTable[18][0].set(outAlphabet::R, 0);
				transitionTable[18][1].set(outAlphabet::RA, 9);
				transitionTable[18][2].set(outAlphabet::RA, 1);
				transitionTable[18][3].set(outAlphabet::R, 7);
				transitionTable[18][8].set(outAlphabet::RA, 8);
				transitionTable[18][17].set(outAlphabet::R, 12);
				transitionTable[18][18].set(outAlphabet::RA, 9);
			}

			state actualState = 0;

			constexpr static std::pair<state, const char *> validStates[] = {
					{1,  "Numero"},
					{3,  "Numero"},
					{6,  "Numero"},
					{7,  "Literal"},
					{8,  "FC_P"},
					{9,  "ID"},
					{11, "PT_V"},
					{12, "EOF"},
					{13, "OPR"},
					{14, "OPR"},
					{15, "RCB"},
					{16, "OPR"},
					{17, "OPM"},
					{18, "AB_P"},
					{19, "NÂO TERMINAL"},
					{20, "TMP VAR"}
			};
		public:
			AFD() { initializeTable(); }

			state getState() { return actualState; }

			std::string dot() {
				std::stringstream ss;
				ss << "digraph AFD{\n";
				for (auto &p :validStates) {
					ss << "Q" << p.first << "[label=\"" << p.second << " Q" << p.first << "\"];\n";
				}
				for (auto s = 0; s < 19; s++) {
					for (auto t = 0; t < 20; t++) {
						auto &transition = transitionTable[s][t];
						if (transition.getAction() != outAlphabet::E)
							ss << "\tQ" << s << "  ->  Q" << transition.getState().value()
								<< " [ label = \"" << t << "\" ];" << std::endl;
					}
				}
				ss << "}";
				return ss.str();
			}

			transitionTableItem<outAlphabet> transition(char input, state actualState) {
				try {
					auto transitionNumberVar = transitionNumber(input);
					if (not transitionNumberVar) {
						std::stringstream ss;
						ss << "Caractere " << input << " não é parte da linguagem definida";
						throw std::runtime_error(ss.str());
					}
					auto ret = transitionTable[actualState][transitionNumberVar.value()];
					if (outAlphabet::error == ret.getAction()) {
						AFD::actualState = 12;
					} else AFD::actualState = ret.getState().value();
					return ret;
				} catch (std::runtime_error &e) {
					throw e;
				}
			};

			transitionTableItem<outAlphabet> transition(char input) { return transition(input, actualState); }

			static std::string getTokenName(state s) {
				for (auto &par : validStates) {
					if (par.first == s) return std::string(par.second);
				}
				throw std::runtime_error(std::string("Invalid State").append(std::to_string(__LINE__)));
			};
		};
	}
