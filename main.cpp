//
// Created by diego on 8/29/17.
//

//#pragma once
#include <iostream>
#include <fstream>

#include "include/syntatic_analyser.hpp"


using namespace COM1;
int main(int argc, char** argv){
	if(argc > 1) {
		SIN::SA sa(argv[1]);
		sa.analise();
		std::fstream fs("output.c", fs.out);
		fs << sa.stringOut();
	} else { std::cout << "No Input File" << std::endl;  return 1; }
	return 0;
}
