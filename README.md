# Algol Compiler

This repository contain an implementation of a compiler frontend(lexical, sintatic and semantic analysis), developed as an assignment for compilers class of Universidade Federal de Goiás, 2017-2

The code is generated in standard C.

The input language was defined by the teacher and there are some files _*.alg_  that has some valid code for that language.

The graph for the state machine of lexical analysis is given in graph.svg

## Compile
`g++ -std=c++17 main.cpp -lpthread -o compiler -I include`

or

`clang -std=c++17 main.cpp -lpthread -o compiler -I include`

or

`mkdir build
cd build
cmake ..
`


`-lpthread` is needed beacause compiler is multithreaded, using a simple producer-consumer pattern between lexical and sintatic analysis.

Tested using Fedora 29, gcc 8.2.1, clang 7.0.0
## Executing

`./compiler filename.alg`

The result is given in _output.c_ file.
When compiling, the compiler generates some info about the process.
